"""
Django settings for asterisk_gui project.
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
import binascii

BASE_DIR = os.path.dirname(os.path.dirname(__file__))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.7/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = os.environ.get(
    'SECRET_KEY',
    binascii.hexlify(
        os.urandom(24)
    )
)

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = os.environ.get('DEBUG', "on") == "on"

TEMPLATE_DEBUG = True

ALLOWED_HOSTS = os.environ.get(
    'ALLOWED_HOSTS',
    "localhost,127.0.0.1"
).split(',')


# Application definition

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    "api"
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    # 'django.contrib.auth.middleware.AuthenticationMiddleware',
    # 'django.contrib.auth.middleware.SessionAuthenticationMiddleware'
)

ROOT_URLCONF = 'asterisk_gui.urls'

WSGI_APPLICATION = 'asterisk_gui.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.7/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}

# Internationalization
# https://docs.djangoproject.com/en/1.7/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.7/howto/static-files/

STATIC_URL = '/static/'
FRONT_DIR = os.path.join(BASE_DIR, 'front')


TEMPLATE_DIRS = [
    os.path.join(FRONT_DIR, 'templates')
]

STATICFILES_DIRS = [
    os.path.join(FRONT_DIR, "assets")
]

CORS_ORIGIN_ALLOW_ALL = True
# APPEND_SLASH = True
