#!/usr/bin/python3
"""
Author: Victor Trejo
Description: Project's urls.
"""
from django.conf.urls import patterns, include, url
from django.contrib import admin


urlpatterns = patterns('',\
    url(r'^admin/', include(admin.site.urls)),\
    url(r'^api/', include('api.urls')),\
    url(r'^$', 'api.views.index')\
)
