import {Enum} from 'enumify';
import React from 'react';

class AlertTypes extends Enum
{

    toString ()
    {

        return this.value;

    }

}

AlertTypes.initEnum({
    DEFAULT: {value: 'default'},
    WARNING: {value: 'warning'},
    INFO: {value: 'info'},
    DANGER:{value: 'danger'},
    SUCCESS:{value: 'success'}
});

class Alert extends React.Component
{
    render ()
    {

        const {type, id} = this.props;
        const className = `alert alert-${type}`;

        return (
            <div className={className} id={id}>
                {this.props.children}
            </div>
        );

    }

}

Alert.types = AlertTypes; 
Alert.propTypes = {
    type: React.PropTypes.oneOfType([
        AlertTypes
    ]),
    id: React.PropTypes.string,
    children: React.PropTypes.oneOfType([
        React.PropTypes.element,
        React.PropTypes.arrayOf(React.PropTypes.element)
    ])
};
Alert.defaultProps = {type: Alert.types.INFO};

export default Alert;
