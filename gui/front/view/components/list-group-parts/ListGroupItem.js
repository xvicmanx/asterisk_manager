import React from 'react';


class Badge extends React.Component
{
    render ()
    {

        return (
             <span className="badge">
                {this.props.children}
             </span>
        );

    }

}

Badge.propTypes = {
    children: React.PropTypes.oneOfType([
        React.PropTypes.node,
        React.PropTypes.arrayOf(React.PropTypes.node)
    ])
};

export default class ListGroupItem extends React.Component
{
    render ()
    {

        const {badge} = this.props;
        let badgeItem = '';

        if (badge !== '')
        {

            badgeItem = <Badge>{badge}</Badge>;

        }

        return (
             <li className="list-group-item">
                {badgeItem}
                {this.props.children}
             </li>
        );

    }

}

ListGroupItem.propTypes = {
    children: React.PropTypes.oneOfType([
        React.PropTypes.node,
        React.PropTypes.arrayOf(React.PropTypes.node)
    ]),
    badge: React.PropTypes.oneOfType([
        React.PropTypes.node,
        React.PropTypes.arrayOf(React.PropTypes.node)
    ])
};
ListGroupItem.defaultProps = {badge: ''};
