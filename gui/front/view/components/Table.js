import Column from './table-parts/Column';
import {Enum} from 'enumify';
import HeaderColumn from './table-parts/HeaderColumn';
import React from 'react';
import Row from './table-parts/Row';
import TableBody from './table-parts/TableBody';
import TableHead from './table-parts/TableHead';


class TableTypes extends Enum
{
    toString ()
    {

        return this.value;

    }

}

TableTypes.initEnum({
    STRIPED: {value:'table-striped'},
    BORDERED: {value:'table-bordered'},
    CONDENSED: {value: 'table-condensed'},
    HOVER: {value: 'table-hover'}
});

class Table extends React.Component
{
    render ()
    {

        let {type} = this.props;

        if ( (type instanceof Array) )
        {

            type = type.join(' ');

        }

        const className = `table ${type}`;

        return (
            <table className={className}>
                {this.props.children}
            </table>
        );

    }

}

const DEFAULT_KEY = 1;

Table.createTable = function (
    head,
    body,
    type = Table.types.BORDERED,
    key = DEFAULT_KEY
) {

    return (
        <Table type={type} key={key}>
            <Table.Head>
                {head}
            </Table.Head>
            <Table.Body>
                {body}
            </Table.Body>
        </Table>
    );

};


Table.rowFromValues = function (values, key = DEFAULT_KEY)
{

    let index = 0;
    let columns = values.map((element) => {

        return (
            <Table.Column key={index++}>
                {element}
            </Table.Column>
        );

    });

    return (
        <Table.Row key={key}>
            {columns}
        </Table.Row>
    );

};


Table.headerRowFromValues = function (values)
{

    let index = 0;
    let columns = values.map((element) => {

        return (
            <Table.HeaderColumn key={index++}>
                {element}
            </Table.HeaderColumn>
        );

    });

    return (
        <Table.Row>
            {columns}
        </Table.Row>
    );

};

Table.types = TableTypes;
Table.propTypes = {
    type: React.PropTypes.oneOfType([
        TableTypes,
        React.PropTypes.arrayOf(TableTypes)
    ]),
    children: React.PropTypes.oneOfType([
        React.PropTypes.element,
        React.PropTypes.arrayOf(React.PropTypes.element)
    ])
};
Table.defaultProps = {type:  Table.types.STRIPED};
Table.Body = TableBody;
Table.Head = TableHead;
Table.HeaderColumn = HeaderColumn;
Table.Row = Row;
Table.Column = Column;

export default Table;
