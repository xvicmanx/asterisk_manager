import React from 'react';

export default class PageHeader extends React.Component
{
    render ()
    {

        let {title} = this.props;

        return (
            <div className="row">
                <div className="col-lg-12">
                    <h1 className="page-header">{title}</h1>
                </div>
            </div>
        );

    }

}

PageHeader.propTypes = {title: React.PropTypes.string.isRequired};
