import ListGroupItem from './list-group-parts/ListGroupItem';
import React from 'react';

class ListGroup extends React.Component
{

    render ()
    {

        return (
            <ul className="list-group">
                {this.props.children}
            </ul>
        );

    }

}

ListGroup.propTypes = {
    children: React.PropTypes.oneOfType([
        ListGroupItem,
        React.PropTypes.arrayOf(
            ListGroupItem
        )
    ])
};
ListGroup.Item = ListGroupItem;

export default ListGroup;
