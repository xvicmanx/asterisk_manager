import React from 'react';

export default class Glyphicon extends React.Component
{
    render ()
    {

        const {type, prefix} = this.props;
        const className = `${prefix} ${prefix}-${type}`;

        return (
            <span className={className}>
                {this.props.children}
            </span>
        );

    }
}

Glyphicon.defaultProps = {prefix: 'glyphicon'};
Glyphicon.propTypes = {
    type: React.PropTypes.string.isRequired,
    prefix: React.PropTypes.string,
    children: React.PropTypes.oneOfType([
        React.PropTypes.element,
        React.PropTypes.arrayOf(React.PropTypes.element)
    ])
};
