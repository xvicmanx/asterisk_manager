import React from 'react';

export default class Row extends React.Component
{

    render ()
    {

        return (
            <div className="row">
                {this.props.children}
            </div>
        );

    }

}

Row.propTypes = {
    children: React.PropTypes.oneOfType([
        React.PropTypes.element,
        React.PropTypes.arrayOf(React.PropTypes.element)
    ])
};
