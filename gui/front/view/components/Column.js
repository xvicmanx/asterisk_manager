import React from 'react';

export default class Column extends React.Component
{
    screenClass (abbreviation, value)
    {

        return `col-${abbreviation}-${value}`;

    }

    pushIfScreenValue (
        classes,
        abbreviation,
        value
    )
    {

        if (value)
        {

            classes.push(this.screenClass(
                abbreviation,
                value
            ));

        }

    }

    render ()
    {

        const {mediumScreen, smallScreen} = this.props;
        const {extraSmallScreen, largeScreen} = this.props;
        const classes = [];

        this.pushIfScreenValue(
            classes,
            'xs',
            extraSmallScreen
        );
        this.pushIfScreenValue(
            classes,
            'sm',
            smallScreen
        );
        this.pushIfScreenValue(
            classes,
            'md',
            mediumScreen
        );
        this.pushIfScreenValue(
            classes,
            'lg',
            largeScreen
        );
        const className = classes.join(' ');

        return (
            <div className={className}>
                {this.props.children}
            </div>
        );

    }
}

const NUMBER_OF_COLUMN_VALUES = 13;
const values = [...Array(NUMBER_OF_COLUMN_VALUES).keys()];

values.shift();

const VALID_COLUMN_VALUES = React.PropTypes.oneOf(values);

Column.propTypes = {
    mediumScreen: VALID_COLUMN_VALUES,
    smallScreen: VALID_COLUMN_VALUES,
    extraSmallScreen: VALID_COLUMN_VALUES,
    largeScreen: VALID_COLUMN_VALUES,
    children: React.PropTypes.oneOfType([
        React.PropTypes.element,
        React.PropTypes.arrayOf(React.PropTypes.element)
    ])
};
