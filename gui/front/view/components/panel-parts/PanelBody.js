import React from 'react';

export default class PanelBody extends React.Component
{

    render ()
    {

        return (
            <div className="panel-body">
                {this.props.children}
            </div>
        );

    }

}

PanelBody.propTypes = {
    children: React.PropTypes.oneOfType([
        React.PropTypes.node,
        React.PropTypes.arrayOf(React.PropTypes.node)
    ])
};
