import React from 'react';

export default class PanelHeading extends React.Component
{

    render ()
    {

        return (
            <div className="panel-heading">
                {this.props.children}
            </div>
        );

    }

}

PanelHeading.propTypes = {
    children: React.PropTypes.oneOfType([
        React.PropTypes.node,
        React.PropTypes.arrayOf(React.PropTypes.node)
    ])
};
