import React from 'react';

export default class TableHead extends React.Component
{

    render ()
    {

        return (
            <thead>
                {this.props.children}
            </thead>
        );

    }

}

TableHead.propTypes = {
    children: React.PropTypes.oneOfType([
        React.PropTypes.node,
        React.PropTypes.arrayOf(React.PropTypes.node)
    ])
};
