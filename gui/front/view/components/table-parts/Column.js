import React from 'react';

export default class Column extends React.Component
{

    render ()
    {

        return (
            <td>
                {this.props.children}
            </td>
        );

    }

}

Column.propTypes = {
    children: React.PropTypes.oneOfType([
        React.PropTypes.node,
        React.PropTypes.arrayOf(React.PropTypes.node)
    ])
};
