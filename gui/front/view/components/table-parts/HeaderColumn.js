import React from 'react';

export default class HeaderColumn extends React.Component
{
    render ()
    {

        return (
            <th>
                {this.props.children}
            </th>
        );

    }

}

HeaderColumn.propTypes = {
    children: React.PropTypes.oneOfType([
        React.PropTypes.node,
        React.PropTypes.arrayOf(React.PropTypes.node)
    ])
};
