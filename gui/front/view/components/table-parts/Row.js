import React from 'react';

export default class Row extends React.Component
{

    render ()
    {

        return (
            <tr>
                {this.props.children}
            </tr>
        );

    }

}

Row.propTypes = {
    children: React.PropTypes.oneOfType([
        React.PropTypes.node,
        React.PropTypes.arrayOf(React.PropTypes.node)
    ])
};
