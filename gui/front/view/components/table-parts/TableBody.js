import React from 'react';

export default class TableBody extends React.Component
{

    render ()
    {

        return (
            <tbody>
                {this.props.children}
            </tbody>
        );

    }

}

TableBody.propTypes = {
    children: React.PropTypes.oneOfType([
        React.PropTypes.node,
        React.PropTypes.arrayOf(React.PropTypes.node)
    ])
};
