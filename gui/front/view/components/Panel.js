import {Enum} from 'enumify';
import PanelBody from './panel-parts/PanelBody';
import PanelHeading from './panel-parts/PanelHeading';
import React from 'react';

class PanelTypes extends Enum
{
    toString ()
    {

        return this.value;

    }

}

PanelTypes.initEnum({
    DEFAULT: {value:'default'},
    WARNING: {value:'warning'},
    INFO: {value: 'info'},
    DANGER: {value: 'danger'},
    SUCCESS: {value: 'success'},
    GREEN: {value: 'green'},
    YELLOW: {value: 'yellow'},
    RED: {value: 'red'},
    PRIMARY: {value: 'primary'}
});


class Panel extends React.Component
{

    render ()
    {

        const {type, id} = this.props;
        const className = `panel panel-${type}`;

        return (
            <div id={id} className={className}>
                {this.props.children}
            </div>
        );

    }

}

Panel.create = function (title, body, type, id)
{

    return (
            <Panel type={type} id={id}>
                <Panel.Heading>{title}</Panel.Heading>
                <Panel.Body>{body}</Panel.Body>
            </Panel>
    );

};

Panel.types = PanelTypes;
Panel.propTypes = {
    type: React.PropTypes.oneOfType([PanelTypes]),
    id: React.PropTypes.string,
    children: React.PropTypes.oneOfType([
        React.PropTypes.element,
        React.PropTypes.arrayOf(React.PropTypes.element)
    ])
};

Panel.defaultProps = {type: Panel.types.DEFAULT};

Panel.Body = PanelBody;
Panel.Heading = PanelHeading;

export default Panel;
