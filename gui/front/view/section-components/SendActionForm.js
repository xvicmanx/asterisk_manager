import React from 'react';
import {connect} from 'react-redux';
import {sendActionRequest} from '../../data/Actions';

class SendActionForm extends React.Component
{
    handleClick ()
    {

        this.props.sendAction(
            this.refs.actionInput.value
        );

    }

    render ()
    {

        return (
            <div>
                <div className="form-group">
                    <label  htmlFor="action-input">Action</label>
                    <input ref="actionInput" type="text" className="form-control" id="action-input" placeholder="Enter a valid action"/>
                </div>
                <button className="btn btn-primary" onClick={this.handleClick.bind(this)}>
                        Send Action
                </button>
            </div>
        );

    }
}

SendActionForm.propTypes = {sendAction: React.PropTypes.func};

/**
 * Passes the updated state as props to the component.
 * @param  {Object} state  updated state.
 * @return {Object}
 */
function mapDispatchToProps (dispatch, ownProps)
{

    return {
        sendAction: (action) => {

            dispatch(
                sendActionRequest(action)
            );

        }
    };

}

export default connect(
    null,
    mapDispatchToProps
)(SendActionForm);
