import Alert from '../components/Alert';
import Glyphicon from '../components/Glyphicon';
import React from 'react';
import Table from '../components/Table';

export default class QueueSummaryTable extends React.Component
{

    field (label, icon)
    {

        return (
            <span key={label}>
                <Glyphicon prefix="fa" type={icon}/> {label}
            </span>
        );

    }

    render ()
    {

        const {Calls, Completed, Abandoned} = this.props;
        const {Holdtime, TalkTime, Strategy} = this.props;
        const {Weight} = this.props;

        const queueSummaryFields = [
            this.field('Calls', 'phone'),
            this.field('Completed', 'thumbs-up'),
            this.field('Abandoned', 'thumbs-down'),
            this.field('Holdtime', 'clock-o'),
            this.field('TalkTime', 'clock-o'),
            this.field('Strategy', 'wrench'),
            this.field('Weight', 'circle')
        ];
        const queueSummaryValues = [
            Calls,
            Completed,
            Abandoned,
            Holdtime,
            TalkTime,
            Strategy,
            Weight
        ];

        return (
            <Alert type={Alert.types.DEFAULT} id="queue-summary">
                {Table.createTable(
                    Table.headerRowFromValues(
                        queueSummaryFields
                    ),
                    Table.rowFromValues(
                        queueSummaryValues
                    ),
                    [
                        Table.types.STRIPED,
                        Table.types.BORDERED
                    ]
                )}
            </Alert>
        );

    }
}

QueueSummaryTable.propTypes = {
    Calls: React.PropTypes.string,
    Completed: React.PropTypes.string,
    Abandoned: React.PropTypes.string,
    Holdtime: React.PropTypes.string,
    TalkTime: React.PropTypes.string,
    Strategy: React.PropTypes.string,
    Weight: React.PropTypes.string
};
