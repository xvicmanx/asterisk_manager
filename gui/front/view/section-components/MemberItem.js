/**
 * Author: Victor Trejo
 * Description: Command Tester Interface Component.
 */

import Column from '../components/Column';
import Glyphicon from '../components/Glyphicon';
import ListGroup from '../components/ListGroup';
import Panel from '../components/Panel';
import React from 'react';

/*
Member status code to panel's
color and label map.
*/
const MemberStatus = [
    {
        label:'UNKNOWN',
        color:Panel.types.DEFAULT
    },
    {
        label:'NOT INUSE',
        color: Panel.types.GREEN
    },
    {
        label:'INUSE',
        color: Panel.types.PRIMARY
    },
    {
        label:'BUSY',
        color: Panel.types.ERROR
    },
    {
        label:'INVALID',
        color: Panel.types.RED
    },
    {
        label:'UNAVAILABLE',
        color: Panel.types.WARNING
    },
    {
        label:'RINGING',
        color: Panel.types.YELLOW
    },
    {
        label:'RINGINUSE',
        color: Panel.types.YELLOW
    },
    {
        label:'ONHOLD',
        color: Panel.types.INFO
    }
];

/**
 * Create a group item's configuration object.
 * @param  {String} badge 
 * @param  {String} label
 * @param  {String} iconType
 * @return {Object}
 */
const groupItemConfig = (
    badge,
    label,
    iconType) => {

    return ({
        badge,
        label,
        iconType
    });

};

/*
Queue's member item component.
 */
export default class MemberItem extends React.Component
{
    /**
     * MemberItem's component content.
     * @return {node}
     */
    content ()
    {

        const {Name} = this.props;
        const {Status, Paused} = this.props;
        const {Membership, LastCall} = this.props;
        const {CallsTaken, Location} = this.props;
        const items = [
            groupItemConfig(
                Name,
                'Name',
                'user'
            ),
            groupItemConfig(
                MemberStatus[Status].label,
                'Status',
                'circle'
            ),
            groupItemConfig(
                Paused,
                'Paused',
                'pause'
            ),
            groupItemConfig(
                Membership,
                'Membership',
                'user'
            ),
            groupItemConfig(
                LastCall,
                'LastCall',
                'phone'
            ),
            groupItemConfig(
                CallsTaken,
                'CallsTaken',
                'phone'
            ),
            groupItemConfig(
                Location,
                'Location',
                'location-arrow'
            )
        ];

        const groupItems = items.map((item) => {

            return (
                <ListGroup.Item badge={item.badge} key={item.label}>
                    <Glyphicon prefix="fa" type={item.iconType}/> {item.label}
                </ListGroup.Item>
            );

        });

        return (
            <ListGroup>{groupItems}</ListGroup>
        );

    }


    /**
     * Component's render method.
     * @return {node}
     */
    render ()
    {

        const {Name, Status} = this.props;

        return (
            <Column mediumScreen={3}>
                {Panel.create(
                        `Member: ${Name}`,
                        this.content(),
                        MemberStatus[Status].color,
                        Name
                )}
            </Column>
        );

    }
}


MemberItem.propTypes = {
    Name: React.PropTypes.string,
    Status: React.PropTypes.string,
    Paused: React.PropTypes.string,
    Membership: React.PropTypes.string,
    LastCall: React.PropTypes.string,
    Location: React.PropTypes.string,
    CallsTaken: React.PropTypes.string
};
