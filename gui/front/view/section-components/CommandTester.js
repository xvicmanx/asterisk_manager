/**
 * Author: Victor Trejo
 * Description: Command Tester Interface Component.
 */
import Alert from '../components/Alert';
import Column from '../components/Column';
import {Link} from 'react-router';
import PageHeader from '../components/PageHeader';
import Panel from '../components/Panel';
import React from 'react';
import Row from '../components/Row';
import SendActionForm from './SendActionForm';
import Table from '../components/Table';
import {connect} from 'react-redux';


/**
 * Command Tester Visual Component.
 */
class CommandTester extends React.Component
{
 
    /**
     * Converts an object to a key-value pairs array.
     * @param  {Object} target the object to be converted to object pairs.
     * @return {Array}
     */
    objectToPairs (target)
    {

        return Object.keys(target).map( (key) => [key, target[key]]);

    }

    /**
     * Creates a table component to represent
     * an object as  key-value  column table.
     * @param  {node} body  the body of the table
     * @param  {String} key  the key of the wrapping alert.
     * @return  {Alert}
     */
    getTable (body, key)
    {

        return (
            <Alert type="info" key={key}>
                <h3>Message {key}</h3>
                {Table.createTable(
                    Table.headerRowFromValues(
                        [
                            'Key',
                            'Value'
                        ]
                    ),
                    body,
                    Table.types.STRIPED,
                    key
                )}
            </Alert>
        );

    }

    /**
     * Creates a Panel component.
     * @param  {String} title  the title of the panel.
     * @param  {node}   body   the body of the panel.
     * @param  {Panel.type}  type  the type of the panel.
     * @param  {int}    size the size of the panel.
     * @param  {String}  id  the id of the panel.
     * @return {Column}
     */
    getBoxPanel (title, body, type, size, id)
    {

        return (
            <Column mediumScreen={size}>
                {Panel.create(
                    title,
                    body,
                    type,
                    id
                )}
            </Column>
        );

    }


    /**
     * Copies an array in reverse.
     * @param  {Array} source  array to be copied in reverse.
     * @return {Array}
     */
    reverse (source)
    {

        const result = Array.prototype.slice.call(source);

        return result.reverse();

    }

    /** Converts a series of objects in to key-value table components.
     * @param  {Array} objects  objects to be converted to tables.
     * @return {Array}
     */
    tablesFromObjects (objects)
    {

        let tableIndex = 0;
        let valuesIndex = 0;

        return this.reverse(objects).map( (object) => {

            return this.getTable(
                this.objectToPairs(object).map( (pair) => {

                    return Table.rowFromValues(
                        pair,
                        valuesIndex++
                    );

                }),
                tableIndex++
            );

        });

    }

    /**
     * Component's render method.
     * @return {node}
     */
    render ()
    {

        const title = 'Command Tester';
        const responses = this.tablesFromObjects(
            this.props.actionResponses
        );
        const events = this.tablesFromObjects(
            this.props.eventsMessages
        );

        const HALF_GRID_SIZE = 6;
        const FULL_GRID_SIZE = 12;
        let responsesBox = this.getBoxPanel(
            'Actions Responses',
            responses,
            Panel.types.PRIMARY,
            HALF_GRID_SIZE,
            'responses-box'
        );

        let eventsBox = this.getBoxPanel(
            'Event Messages',
            events,
            Panel.types.PRIMARY,
            FULL_GRID_SIZE,
            'events-box'
        );

        let formBox = this.getBoxPanel(
            'Interface to send Actions',
            <SendActionForm/>,
            Panel.types.PRIMARY,
            HALF_GRID_SIZE,
            'send-actions-box'
        );

        return (
            <div id="command-tester">
                <PageHeader title={title} />
                <Link to="/">Queues summary</Link>
                <Row>
                    {formBox}
                    {responsesBox}
                </Row>
                <Row>{eventsBox}</Row>
            </div>
        );

    }

}

CommandTester.propTypes = {
    actionResponses: React.PropTypes.arrayOf(
            Object
    ),
    eventsMessages: React.PropTypes.arrayOf(
            Object
    )
};

/**
 * Passes the updated state as props to the component.
 * @param  {Object} state  updated state.
 * @return {Object}
 */
function mapStateToProps (state) {
  
    return {
        actionResponses: state.actionResponses,
        eventsMessages: state.eventsMessages
    };

}


export default connect(
    mapStateToProps
)(CommandTester);
