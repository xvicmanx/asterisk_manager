/**
 * Author: Victor Trejo
 * Description: Queue Details Section View Component.
 */

import {Link, withRouter} from 'react-router';
import MemberItem from './MemberItem';
import PageHeader from '../components/PageHeader';
import QueueSummaryTable from './QueueSummaryTable';
import React from 'react';
import Row from '../components/Row';
import {connect} from 'react-redux';


class QueueDetails extends React.Component
{

    componentWillMount ()
    {

        this.queueName = this.props.params.queueName;
        const queue = this.props.queues[this.queueName];

        if ( !queue )
        {

            this.props.router.push('/');

        }

    }

    render ()
    {

        const title = `Queue ${this.queueName} details`;
        const queue = this.props.queues[this.queueName];
        let items = [];

        if ( !queue )
        {

            return (
                <div></div>
            );

        }

        const members = this.props.members.filter((member) => {

            return member.Queue === this.queueName;

        });


        for ( let memberName in members )
        {

            items.push(
                <MemberItem
                            key={memberName}
                            {...members[memberName]}
                />
            );

        }

        return (
            <div>
                <PageHeader title={title} />
                <QueueSummaryTable {...queue} />
                <h3>Queue Members</h3> <hr/>
                <Row>{items}</Row>
                <Row>
                    <Link to="/queues-summary">Back to queues summary</Link>
                </Row>
            </div>
        );

    }

}

QueueDetails.propTypes = {
    params: React.PropTypes.shape({queueName: React.PropTypes.string}),
    queues: React.PropTypes.object,
    members: React.PropTypes.array,
    router: React.PropTypes.shape({push: React.PropTypes.func})
};

/**
 * Passes the updated state as props to the component.
 * @param  {Object} state  updated state.
 * @return {Object}
 */
function mapStateToProps (state) {

    return {
        queues: state.queues,
        members: state.members
    };

}


export default withRouter(connect(
    mapStateToProps
)(QueueDetails));
