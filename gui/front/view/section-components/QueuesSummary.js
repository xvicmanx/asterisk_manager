import PageHeader from '../components/PageHeader';
import QueuesSummaryItem from './QueueSummaryItem';
import React from 'react';
import Row from '../components/Row';
import {connect} from 'react-redux';


class QueuesSummary extends React.Component
{
    render ()
    {

        const {queues} = this.props;
        const items = Object.keys(queues).map((key) => {

            return (
                <QueuesSummaryItem key={key}
                                    {...queues[key]}
                />
            );

        });

        return (
            <div>
                <PageHeader title="Queues Summary" />
                <Row>{items}</Row>
            </div>
        );

    }

}

QueuesSummary.propTypes = {queues: React.PropTypes.object};

/**
 * Passes the updated state as props to the component.
 * @param  {Object} state  updated state.
 * @return {Object}
 */
function mapStateToProps (state)
{

    return {queues: state.queues};

}

export default connect(
    mapStateToProps
)(QueuesSummary);

