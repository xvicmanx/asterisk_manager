import Column from '../components/Column';
import Glyphicon from '../components/Glyphicon';
import {Link} from 'react-router';
import ListGroup from '../components/ListGroup';
import Panel from '../components/Panel';
import React from 'react';


export default class QueueSummaryItem extends React.Component
{

    render ()
    {

        const {Queue} = this.props;
        const {Holdtime, Calls} = this.props;
        const {Completed, Abandoned, TalkTime} = this.props;
        const items = [
            {
                badge: Calls,
                label: 'Calls',
                iconType:'phone'
            },
            {
                badge: Completed,
                label: 'Completed',
                iconType: 'thumbs-up'
            },
            {
                badge: Abandoned,
                label: 'Abandoned',
                iconType: 'thumbs-down'
            },
            {
                badge: Holdtime,
                label: 'Hold Time',
                iconType: 'clock-o'
            },
            {
                badge: TalkTime,
                label: 'Talk Time',
                iconType: 'clock-o'
            }
        ];

        const body = items.map((item) => {

            return (
                <ListGroup.Item badge={item.badge} key={item.label}>
                    <Glyphicon prefix="fa" type={item.iconType} /> {item.label}
                </ListGroup.Item>
            );

        });

        return (
            <Column mediumScreen={4}>
                <Link to={`/queue-details/${Queue}`} >
                    <Panel type={Panel.types.INFO} id={Queue}>
                        <Panel.Heading>
                            {`Queue: ${Queue}`}
                        </Panel.Heading>
                        <Panel.Body>
                            <ListGroup>
                                {body}
                            </ListGroup>
                        </Panel.Body>
                    </Panel>
                </Link>
            </Column>
        );

    }
}

QueueSummaryItem.propTypes = {
    Calls: React.PropTypes.string,
    Completed: React.PropTypes.string,
    Abandoned: React.PropTypes.string,
    Holdtime: React.PropTypes.string,
    TalkTime: React.PropTypes.string,
    Queue: React.PropTypes.string
};
