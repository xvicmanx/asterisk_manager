import React from 'react';

export default class NavBarHeader extends React.Component
{
    render ()
    {

        let {logoUrl, logoTitle} = this.props;

        return (
            <div className="navbar-header">
                <button
                        type="button"
                        className="navbar-toggle"
                        data-toggle="collapse"
                        data-target=".navbar-collapse"
                >
                            <span className="icon-bar"></span>
                            <span className="icon-bar"></span>
                            <span className="icon-bar"></span>
                </button>
                <a className="navbar-brand" href={logoUrl}>
                    {logoTitle}
                </a>
            </div>
        );

    }

}

NavBarHeader.propTypes = {
    logoUrl: React.PropTypes.string,
    logoTitle: React.PropTypes.string
};
