import React from 'react';

export default class Content extends React.Component
{
    render ()
    {

        return (
            <div id="page-wrapper">
                {this.props.children}
            </div>
        );

    }

}

Content.propTypes = {
    children: React.PropTypes.oneOfType([
        React.PropTypes.element,
        React.PropTypes.arrayOf(React.PropTypes.element)
    ])
};

