import Content from './Content';
import Header from './Header';
import React from 'react';

const URL = '/queues-summary';
const TITLE = 'Asterisk Realtime admin';

export default class Application extends React.Component
{
    render ()
    {

        return (
            <div id="wrapper">
                <Header  logoUrl={URL} logoTitle={TITLE} />
                <Content>{this.props.children}</Content>
            </div>
        );

    }
}

Application.propTypes = {
    children: React.PropTypes.oneOfType([
        React.PropTypes.element,
        React.PropTypes.arrayOf(React.PropTypes.element)
    ])
};
