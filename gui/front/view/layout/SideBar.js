import Glyphicon from '../components/Glyphicon';
import {Link} from 'react-router';
import React from 'react';


export default class SideBar extends React.Component
{

    render ()
    {

        return (
            <div className="navbar-default sidebar" role="navigation">
                <div className="sidebar-nav navbar-collapse">
                    <ul className="nav in" id="side-menu">
                        <li>
                            <Link to="/command-tester">
                                <Glyphicon prefix="fa" type="paper-plane-o"/> Command Tester
                            </Link>
                        </li>
                        <li>
                            <Link to="/">
                                <Glyphicon prefix="fa" type="dashboard"/> Queues Summary
                            </Link>
                        </li>
                    </ul>
                </div>
            </div>
        );

    }

}
