import NavBarHeader from './NavBarHeader';
import React from 'react';
import SideBar from './SideBar';

export default class Header extends React.Component
{
    render ()
    {

        return (
            <div id="header">
                <nav
                        className="navbar navbar-default navbar-static-top"
                        role="navigation"
                >
                            <NavBarHeader {...this.props} />
                            <SideBar />
                </nav>
            </div>
        );

    }

}
