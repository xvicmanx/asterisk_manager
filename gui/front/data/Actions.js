import Constants from './Constants';
import axios from 'axios';
import cookie from 'react-cookie';


export const registerToEvents = (events) => {

    return (dispatch) => {

        dispatch({
            type: Constants.DISPATCH_EVENTS.ASTERISK_EVENTS_REGISTERED,
            payload: events
        });

        dispatch(
            sendRequest(
                Constants.URLS.REGISTER_TO_EVENTS_POINT,
                {events}
            )
        );

    };

};


export const registerToEvent = (event) => {

    return (dispatch) => {

        dispatch({
            type: Constants.DISPATCH_EVENTS.ASTERISK_EVENT_REGISTERED,
            payload: event
        });

        dispatch(
            sendRequest(
                Constants.URLS.REGISTER_TO_EVENT_POINT,
                {event}
            )
        );

    };

};


export const sendRequest = (url, params = {}) => {

    return (dispatch) => {

        const config = {
            headers: {
                [Constants.CSRF_REQUEST_HEADER]: cookie.load(Constants.CSRF_TOKEN),
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            }
        };

        dispatch({
            type: Constants.DISPATCH_EVENTS.POST_REQUEST_START,
            payload: {
                url,
                params
            }
        });

        axios.post(url, params, config).then((data) => {

            dispatch({
                type: Constants.DISPATCH_EVENTS.POST_REQUEST_SUCESS,
                payload: {
                    url,
                    params,
                    data
                }
            });

        }).catch((data) => {

            dispatch({
                type: Constants.DISPATCH_EVENTS.POST_REQUEST_ERROR,
                payload: {
                    url,
                    params,
                    data
                }
            });

        });

    };

};


export const sendActionRequest = (action) => {

    return (dispatch) => {

        dispatch({
            type: Constants.ACTION_SENT(action)
        });

        dispatch(
            sendRequest(
                Constants.URLS.SEND_ACTION_POINT,
                {
                    action,
                    actionId: (new Date()).getTime()
                }
            )
        );

    };

};


export const webSocketOnMessageReceived = (result) => {

    return {
        type: Constants.MESSAGE_RECEIVED(
            result.notificationType
        ),
        payload: result.data
    };

};

export const webSocketOnOpen = (event) => {

    return (dispatch) => {

        dispatch({type: Constants.DISPATCH_EVENTS.SOCKET_OPENED});

        dispatch(
            registerToEvents(
                Object.keys(Constants.ASTERISK_ACTIONS).map((key) => {

                    return Constants.ASTERISK_ACTIONS[key];

                })
            )
        );

    };

};
