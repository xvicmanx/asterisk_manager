import React from 'react';


export default class WebSocketComponent extends React.Component
{
    componentWillMount ()
    {

        const {url, onMessage, onOpen} = this.props;
        const socket = new WebSocket(url);

        socket.onmessage = (event) => {

            onMessage(JSON.parse(event.data));

        };

        socket.onopen = onOpen;

    }

    render ()
    {

        return <div></div>;

    }
}

WebSocketComponent.propTypes = {
    url: React.PropTypes.string,
    onMessage: React.PropTypes.func,
    onOpen: React.PropTypes.func,
    children: React.PropTypes.oneOfType([
        React.PropTypes.node,
        React.PropTypes.arrayOf(React.PropTypes.node)
    ])
};
