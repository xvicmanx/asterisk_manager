import Constants from '../Constants';
import Handler from './Handler';
import {getProperty} from '../Utils.js';

const NOT_FOUND_INDEX = -1;

export default class MembersHandler extends Handler
{
    matches (action)
    {

        const message = action.payload;
        const isEvent = action.type === Constants.DISPATCH_EVENTS.EVENT_RECEIVED;
        const eventType = getProperty(message, Constants.EVENT_KEY);

        return isEvent && MembersHandler.validEvents.indexOf(
            eventType
        ) !== NOT_FOUND_INDEX;

    }

    updateMembers (members, member)
    {

        const result = [...members];
        let found = false;

        for (const key in result)
        {

            if ( result[key].Name === member.Name )
            {

                result[key] = member;
                found = true;

            }

        }

        if ( !found )
        {

            result.push(member);

        }

        return result;

    }

    newState (state, action)
    {

        const member = action.payload;

        if ( !member.Name )
        {

            member.Name = member.MemberName;

        }

        return this.updateMembers(
            state,
            member
        );

    }
}

MembersHandler.validEvents = [
    Constants.ASTERISK_ACTIONS.QUEUE_MEMBER,
    Constants.ASTERISK_ACTIONS.QUEUE_MEMBER_STATUS
];
