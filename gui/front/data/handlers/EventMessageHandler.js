import Constants from '../Constants';
import Handler from './Handler';


const removeFirstAndPush = (source, element) => {

    const copy = [...source];

    copy.shift();
    copy.push(element);

    return copy;

};

const MAXIMUM_ALLOWED_EVENTS = 30;

export default class EventMessageHandler extends Handler
{

    matches (action)
    {

        return action.type === Constants.DISPATCH_EVENTS.EVENT_RECEIVED;

    }

    newState (state, action)
    {

        let result = null;

        if (state.length > MAXIMUM_ALLOWED_EVENTS)
        {

            result = removeFirstAndPush(
                state,
                action.payload
            );

        }
        else
        {

            result = [...state, action.payload];

        }

        return result;

    }

}
