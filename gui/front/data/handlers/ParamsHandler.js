import Constants from '../Constants';
import Handler from './Handler';
import {getProperty} from '../Utils.js';

export default class ParamsHandler extends Handler
{
    matches (action)
    {

        const message = action.payload;
        const isEvent = action.type === Constants.DISPATCH_EVENTS.EVENT_RECEIVED;
        const eventType = getProperty(message, Constants.EVENT_KEY);
        const isQueueParamsEvent = eventType === Constants.ASTERISK_ACTIONS.QUEUE_PARAMS;

        return isEvent && isQueueParamsEvent;

    }

    newState (state, action)
    {

        const message = action.payload;
        const source = {[message.Queue]: message};

        return Object.assign(
            {},
            state,
            source
        );

    }

}
