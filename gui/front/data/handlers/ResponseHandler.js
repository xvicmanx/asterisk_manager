import Handler from './Handler';
import Constants from '../Constants';

const removeFirstAndPush = (source, element) => {

    const copy = [...source];

    copy.shift();
    copy.push(element);

    return copy;

};

const MAXIMUM_NUMBER_OF_RESPONSES = 10;

export default class ResponseHandler extends Handler
{
    matches (action)
    {

        return action.type === Constants.DISPATCH_EVENTS.RESPONSE_RECEIVED;

    }

    newState (state, action)
    {

        let result = state;

        if ( state.length > MAXIMUM_NUMBER_OF_RESPONSES )
        {

            result = removeFirstAndPush(
                state,
                action.payload
            );

        }
        else
        {

            result = [...state, action.payload];

        }

        return result;

    }

}
