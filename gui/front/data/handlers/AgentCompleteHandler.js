import Constants from '../Constants';
import Handler from './Handler';
import {getProperty} from '../Utils.js';

const INCREMENT = 1;
const DECIMAL_RADIX = 10;

export default class AgentCompleteHandler extends Handler
{
    matches (action)
    {

        const message = action.payload;
        const isEvent = action.type === Constants.DISPATCH_EVENTS.EVENT_RECEIVED;
        const eventType = getProperty(message, Constants.EVENT_KEY);
        const isCompleteEvent = eventType === Constants.ASTERISK_ACTIONS.AGENT_COMPLETE;

        return isEvent && isCompleteEvent;

    }

    newState (state, action)
    {

        const message = action.payload;
        const result = Object.assign({}, state);
        const completed = result[message.Queue].Completed;

        result[message.Queue].Completed = parseInt(completed, DECIMAL_RADIX) + INCREMENT;

        return result;

    }
}
