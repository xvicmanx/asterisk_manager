import Constants from '../Constants';
import Handler from './Handler';
import {getProperty} from '../Utils.js';

const INCREMENT = 1;
const DECIMAL_RADIX = 10;

export default class AbandonedHandler extends Handler
{

    matches (action)
    {

        const message = action.payload;
        const isEvent = action.type === Constants.DISPATCH_EVENTS.EVENT_RECEIVED;
        const eventType = getProperty(message, Constants.EVENT_KEY);
        const isAbandonedEvent = eventType === Constants.ASTERISK_ACTIONS.QUEUE_CALLER_ABANDON;

        return isEvent && isAbandonedEvent;

    }

    newState (state, action)
    {

        const message = action.payload;
        const result = Object.assign({}, state);
        const abandoned = result[message.Queue].Abandoned;

        result[message.Queue].Abandoned = parseInt(abandoned, DECIMAL_RADIX) + INCREMENT;

        return result;

    }
}

