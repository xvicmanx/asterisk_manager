export const getProperty = (object, propertyName, defaultValue = null) => {

    if ( typeof object != 'object' ||
        typeof object[propertyName] == 'undefined'
    )
    {

        return defaultValue;

    }

    return object[propertyName];

};