import AbandonedHandler from './handlers/AbandonedHandler';
import AgentCompleteHandler from './handlers/AgentCompleteHandler';
import EventMessageHandler from './handlers/EventMessageHandler';
import MembersHandler from './handlers/MembersHandler';
import ParamsHandler from './handlers/ParamsHandler';
import ResponseHandler from './handlers/ResponseHandler';
import {combineReducers} from 'redux';


const createReducer = (handlers, defaultValue = {}) => {

    return (state = defaultValue, action) => {

        let result = state;

        for (const key in handlers)
        {

            const handler = handlers[key];

            if ( handler.matches(action) )
            {

                result = handler.newState(
                    result,
                    action
                );

            }

        }

        return result;

    };

};


const handlers = {
    queues: [
        new ParamsHandler(),
        new AbandonedHandler(),
        new AgentCompleteHandler()
    ],
    members: [new MembersHandler()],
    events: [new EventMessageHandler()],
    responses: [new ResponseHandler()]
};

export const reducers = combineReducers(
    {
        queues: createReducer(handlers.queues),
        members: createReducer(
            handlers.members,
            []
        ),
        actionResponses: createReducer(
            handlers.responses,
            []
        ),
        eventsMessages: createReducer(
            handlers.events,
            []
        )
    }
);
