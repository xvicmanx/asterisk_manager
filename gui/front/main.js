import {IndexRoute, Route, Router, hashHistory} from 'react-router';
import {applyMiddleware, createStore} from 'redux';
import {sendActionRequest, webSocketOnMessageReceived, webSocketOnOpen} from './data/Actions';
import Application from './view/layout/Application';
import CommandTester from './view/section-components/CommandTester';
import Constants from './data/Constants';
import {Provider} from 'react-redux';
import QueueDetails from './view/section-components/QueueDetails';
import QueuesSummary from './view/section-components/QueuesSummary';
import React from 'react';
import ReactDom from 'react-dom';
import WebSocketComponent from './data/WebSocketComponent';
import createLogger from 'redux-logger';
import {reducers} from './data/Reducers';
import thunk from 'redux-thunk';


const store = createStore(
    reducers,
    applyMiddleware(
        thunk,
        createLogger()
    )
);

const socketUrl = document.getElementById(Constants.WEBSOCKET_URL_KEY).value;

ReactDom.render(
    <Provider store={store}>
        <div>
            <Router history={hashHistory}>
                <Route path="/" component={Application}>
                    <IndexRoute component={QueuesSummary} />
                    <Route path="command-tester" component={CommandTester} />
                    <Route path="queue-details/:queueName" component={QueueDetails} />
                </Route>
            </Router>

            <WebSocketComponent url = {socketUrl}
                  onMessage = {

                        (data) => {

                            store.dispatch(
                                webSocketOnMessageReceived(data)
                            );

                        }

                    }

                  onOpen = {

                        (data) => {

                            store.dispatch((dispatch) => {

                                dispatch(webSocketOnOpen(data));
                                dispatch(
                                    sendActionRequest(
                                        Constants.ASTERISK_ACTIONS.QUEUE_STATUS
                                    )
                                );

                            });

                        }

                   }
            />
        </div>
    </Provider>,
    document.getElementById(Constants.ASTERISK_APP_ID)
);

