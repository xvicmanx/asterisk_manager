
var connectButton = document.getElementById('connect');

function getContent(title, description)
{
	return "<div><h2>"+title+"</h2>"+
		  				"<pre>"+ description + "</pre></div>";
}

var socket;
var textArea = document.getElementById('result');
connectButton.addEventListener(
	'click',
	function(){
		var url = "ws://localhost:5000/socket";	
		socket = new WebSocket(url);

		textArea.innerHTML+= getContent(
		  		"Connecting",
		  		url
		 );

		socket.onmessage = function(event) {
			  var result = JSON.parse(event.data);
			  textArea.innerHTML+= getContent(
			  		result.notificationType,
			  		JSON.stringify(result.data)
			  );
		};

		socket.onopen = function (event) {
		 	textArea.innerHTML+= getContent(
			  		"Connected!",
			  		url
			);
		};
});


function request(url) {
	  var xhttp = new XMLHttpRequest();
	  xhttp.onreadystatechange = function() {
	    if (xhttp.readyState == 4 && xhttp.status == 200) {
	      	console.log("success")
	    }
	  };
	  xhttp.open("GET", url, true);
	  xhttp.send();
}


var pingButton = document.getElementById('ping');
pingButton.addEventListener(
	'click',
	function(){
		var pingUrl = "http://localhost:82/send-action?action=Ping&actionId=189&url=http://localhost:8000/responses-listener"
		textArea.innerHTML+= getContent(
		  		"Sending Ping!",
		  		pingUrl
		);
		request(pingUrl);
});



