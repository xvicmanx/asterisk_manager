#!/usr/bin/python3
"""
Author: Victor Trejo
Description: Custom template tags.
"""
from django import template
from .. import settings

print(dir(settings))

register = template.Library()

@register.simple_tag
def settings_property(name):
    """
    Gets the value of a property from application
    the settings.
    param name: name of the property.
    type name: string
    return: the value of the property.
    rtype: object
    """
    return getattr(settings, name)
