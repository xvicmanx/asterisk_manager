#!/usr/bin/python3
"""
Author: Victor Trejo
Description: API forms.
"""
from django import forms
from . import settings

CHAR_FIELD = forms.CharField(
    min_length=4,
    max_length=100
)
class EventRegistrationForm(forms.Form):
    """Form to validate request of event registration."""
    event = CHAR_FIELD

class SendingActionsForm(forms.Form):
    """Form to validate request of sending actions."""
    action = CHAR_FIELD
    actionId = forms.IntegerField(min_value=0)



class EventsRegistrationForm(forms.Form):
    """Form to validate request of events registration."""

    def clean(self):
        """
        Form clean method.
        """
        super(EventsRegistrationForm, self).clean()

        if settings.EVENTS_KEY in self.data:
            events = self.data.get(settings.EVENTS_KEY, [])
            for event in events:
                try:
                    CHAR_FIELD.run_validators(event)
                except forms.ValidationError:
                    raise forms.ValidationError(
                        'Not all the values in {} are valid'.format(
                            settings.EVENTS_KEY
                        )
                    )
            self.cleaned_data[settings.EVENTS_KEY] = events
        else:
            raise forms.ValidationError(
                'events key missing'
            )
        return self.cleaned_data
