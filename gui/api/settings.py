"""
Django settings for api app.
"""
import os

HOST_NAME = os.environ.get(
	'HOST_NAME',
	"localhost"
)

WEBSOCKET_PORT = os.environ.get(
	'WEBSOCKET_PORT',
	5000
)

WEBSOCKET_URL = 'ws://{}:{}/socket'.format(
	HOST_NAME,
	WEBSOCKET_PORT
)
WEB_SOCKETS_NOTIFICATIONS_POINT = "http://{}:{}/notify".format(
	HOST_NAME,
	WEBSOCKET_PORT
)

WEB_SERVER_PORT = os.environ.get(
	'WEB_SERVER_PORT',
	8000
)
HOST = "http://{}:{}".format(
	HOST_NAME,
	WEB_SERVER_PORT
)

ASTERISK_SERVER_PORT = os.environ.get(
	'ASTERISK_SERVER_PORT',
	82
)
ASTERISK_SERVER = "http://{}:{}".format(
	HOST_NAME,
	ASTERISK_SERVER_PORT
)

SEND_ACTION_POINT = "{}/send-action".format(ASTERISK_SERVER)
REGISTER_TO_EVENT_POINT = "{}/register-to-event".format(ASTERISK_SERVER)
REGISTER_TO_EVENTS_POINT = "{}/register-to-events".format(ASTERISK_SERVER)
RESPONSE_LISTENER_POINT = "{}/api/responses-listener".format(HOST)
EVENTS_LISTENER_POINT = "{}/api/events-listener".format(HOST)
ACTION_KEY = 'action'
ACTION_ID = 'actionId'
URL_KEY = 'url'
EVENT_KEY = "event"
EVENTS_KEY = "events"
EVENTS_KEY_SEND = "events[]"
