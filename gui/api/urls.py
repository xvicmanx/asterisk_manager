#!/usr/bin/python3
"""
Author: Victor Trejo
Description: API urls.
"""
from django.conf.urls import patterns, url
from . import views

urlpatterns = patterns('',\
    url(r'^events-listener/', views.listen_to_events),\
    url(r'^responses-listener/', views.listen_to_responses),\
    url(r'^send-action/', views.send_action),\
    url(r'^register-to-event/', views.register_to_event),\
    url(r'^register-to-events/', views.register_to_events)\
)
