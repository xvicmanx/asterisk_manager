#!/usr/bin/python3
"""
Author: Victor Trejo
Description: API views.
"""
import json
import requests
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
from django.shortcuts import render
from django.views.decorators.csrf import ensure_csrf_cookie
from . import settings
from . import forms

UNSUCCESS_RESPONSE = {
    "success": False,
    "reason": "Invalid params!"
}
SUCCESS_RESPONSE = {
    "success": True
}
BAD_REQUEST_STATUS = 400
OK_REQUEST_STATUS = 200
def __validates(form_type):
    def __decorator(function):
        def __wrapper(request):
            data = json.loads(
                request.body.decode('utf-8')
            )
            form = form_type(dict(data))
            response = UNSUCCESS_RESPONSE.copy()
            status = BAD_REQUEST_STATUS
            if form.is_valid():
                function(form.cleaned_data)
                response = SUCCESS_RESPONSE
                status = OK_REQUEST_STATUS
            else:
                response["reason"] = form.errors.as_json()
            return JsonResponse(response, status=status)
        return __wrapper
    return __decorator



def __notify(\
    notification_type,\
    data\
):
    """
    Communicates to the websocket server to
    notify all the websocket clients.

    param notification_type: the type of notification.
    type notificatiton_type: string.

    param data: the notification's data.
    type data: dictionary.
    """
    requests.post(
        settings.WEB_SOCKETS_NOTIFICATIONS_POINT,
        data={
            'notificationType': notification_type,
            'data': data
        }
    )


@require_http_methods(["GET", "POST"])
def listen_to_events(request):
    """
    Listen to events view.
    """
    data = request.GET
    __notify(
        "Event",
        json.dumps(data.dict())
    )
    return JsonResponse(
        SUCCESS_RESPONSE
    )


@require_http_methods(["GET", "POST"])
def listen_to_responses(request):
    """
    Listen to responses view.
    """
    data = request.GET
    __notify(
        "Response",
        json.dumps(data.dict())
    )
    return JsonResponse(
        SUCCESS_RESPONSE
    )



@require_http_methods(["POST"])
@__validates(forms.SendingActionsForm)
def send_action(data):
    """
    Send action view.
    """
    requests.get(
        settings.SEND_ACTION_POINT,
        params={
            settings.ACTION_KEY: data.get(
                settings.ACTION_KEY
            ),
            settings.ACTION_ID: data.get(
                settings.ACTION_ID
            ),
            settings.URL_KEY: settings.RESPONSE_LISTENER_POINT
        }
    )


@require_http_methods(["POST"])
@__validates(forms.EventsRegistrationForm)
def register_to_events(data):
    """
    Register to events view.
    """
    requests.get(
        settings.REGISTER_TO_EVENTS_POINT,
        params={
            settings.EVENTS_KEY_SEND: data.get(
                settings.EVENTS_KEY
            ),
            settings.URL_KEY: settings.EVENTS_LISTENER_POINT
        }
    )



@require_http_methods(["POST"])
@__validates(forms.EventRegistrationForm)
def register_to_event(data):
    """
    Register to event view.
    """
    requests.get(
        settings.REGISTER_TO_EVENT_POINT,
        params={
            settings.EVENT_KEY: data.get(
                settings.EVENT_KEY
            ),
            settings.URL_KEY: settings.EVENTS_LISTENER_POINT
        }
    )

@ensure_csrf_cookie
def index(request):
    """
    Main page view
    """
    return render(request, "index.html", {})
