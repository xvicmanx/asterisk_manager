#!/usr/bin/python3
"""
Author: Victor Trejo
Description: Contains all the action messages objects for the asterisk manager.
"""
import json
import signal
import logging
from urllib.parse import urlparse
from tornado.ioloop import IOLoop
from tornado.websocket import WebSocketHandler
from tornado.websocket import WebSocketClosedError
from tornado.web import Application
from tornado.web import RequestHandler
from tornado.options import define
from tornado.options import parse_command_line
from tornado.options import options

define(
    'debug',
    default=False,
    type=bool,
    help='Run in debug mode'
)
define(
    'port',
    default=5000,
    type=int,
    help='Server port'
)
define(
    'allowed_hosts',
    default="localhost:8000",
    multiple=True,
    help='Allowed hosts'
)


def log_end(message):
    """
    Logs the end of the function call.
    param message: message to print out on the screen.
    type message: string
    """
    def __decorator(function):
        def __wrapper(*args, **kwargs):
            result = function(*args, **kwargs)
            logging.info(message)
            return result
        return __wrapper
    return __decorator


class AsteriskWebSocketHandler(WebSocketHandler):
    """docstring for AsteriskWebSocket"""
    count = 0
    def __init__(self, *args, **kargs):
        super(AsteriskWebSocketHandler, self).__init__(
            *args,
            **kargs
        )
        AsteriskWebSocketHandler.count += 1
        self.identifier = AsteriskWebSocketHandler.count

    @log_end("The WebSocket  is opened")
    def open(self):
        """
        Open websocket event handler.
        """
        self.application.add_client(
            self.identifier,
            self
        )

    @log_end("Writing message")
    def on_message(self, message):
        """
        On Message websocket event handler.
        """
        self.write_message(message)

    @log_end("The WebSocket is closed")
    def on_close(self):
        """
        On close websocket event handler.
        """
        self.application.remove_client(
            self.identifier
        )

    def check_origin(self, origin):
        """
        Checks if the given origin is an allowed one.
        param origin: origin to be checked.
        type origin: string
        return: True if the origin is allowed
        rtype: bool
        """
        allowed = super().check_origin(origin)
        parsed = urlparse(origin.lower())
        hosts = options.allowed_hosts
        hosts = (hosts, ) if isinstance(hosts, str) else hosts
        matched = any(parsed.netloc == host for host in hosts)
        return options.debug or allowed or matched



class AsteriskNotificationsHandler(RequestHandler):
    """AsteriskNotificationsHandler"""

    NOTIFICATION_TYPE_KEY = 'notificationType'
    DATA_KEY = 'data'

    def __broadcast_with_args(self):
        self.__broadcast_message(
            self.get_argument(
                self.NOTIFICATION_TYPE_KEY
            ),
            self.get_argument(self.DATA_KEY)
        )

    @log_end("A POST request made to the request handler!")
    def post(self, *args, **kargs):
        """
        POST request handler method.
        """
        self.__broadcast_with_args()

    @log_end("A GET request made to the request handler!")
    def get(self, *args, **kargs):
        """
        GET request handler method.
        """
        self.__broadcast_with_args()

    def __send_message_to_client(self, message, client):
        try:
            client.write_message(message)
        except WebSocketClosedError:
            self.application.remove_client(
                client.identifier
            )

    def __broadcast_message(self, notification_type, data):
        message = json.dumps({
            self.DATA_KEY: dict(json.loads(data)),
            self.NOTIFICATION_TYPE_KEY:notification_type
        })

        for client in self.application.get_clients():
            self.__send_message_to_client(
                message,
                client
            )



class AsteriskApplication(Application):
    """
    Websocket application.
    """
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.__websocket_clients = {}

    @log_end("Adding a client")
    def add_client(\
        self,\
        identifier,\
        client\
    ):
        """
        Adds a websocket client identified
        by a given identifier.

        param identifier: client's identifier.
        type identifier: string.
        """
        self.__websocket_clients[identifier] = client

    @log_end("Removing a client")
    def remove_client(\
        self,\
        identifier\
    ):
        """
        Removes a websocket client identified
        by a given identifier.

        param identifier: client's identifier.
        type identifier: string.
        """
        del self.__websocket_clients[identifier]

    def get_clients(self):
        """
        Gets the connected websocket clients.
        return: the connected websocket clients.
        rtype: an enumerable.
        """
        for client in self.__websocket_clients.values():
            yield client



@log_end("Bye!!!")
@log_end("Shutting down...")
def shutdown():
    """
    Stops the running Asterisk Application
    """
    IOLoop.instance().stop()


def main(port, debug):
    """
    Main function.
    Starts websocket server.
    param port: the port to bind the server.
    type port: int
    param debug: to indicate if debugging is on.
    type debug: boolean
    """
    application = AsteriskApplication([
        (r'/socket', AsteriskWebSocketHandler),
        (r'/notify', AsteriskNotificationsHandler)
    ], debug=debug)
    application.listen(port)
    signal.signal(
        signal.SIGINT,
        lambda sig, frame: shutdown()
    )
    IOLoop.instance().start()




if __name__ == '__main__':
    parse_command_line()
    main(\
        options.port,\
        options.debug\
    )
