#!/usr/bin/python3
"""
Author: Victor Trejo
Description: Contains all the action messages objects for the asterisk manager.
"""
import inspect

class ActionMessage(object):
    """
    docstring for ActionMessage.
    It is an Action Message that can be sent
    to the Asterisk Manager.
    """
    def __init__(self, action, actionId):
        """
        Constructor.

        :param action: Name or type of the action.
        :type action: string.

        :param actionId: Id of the action.
        :type actionId: string.
        """
        super(ActionMessage, self).__init__()
        self.params = {}
        self.params['ActionID'] = actionId
        self.params['Action'] = action

    def __getattr__(self, name):
        """
        To call the object's attributes dinamically.

        :return: the required attribute.
        :rtype: object.
        """
        return  self.params[name]

    def __str__(self):
        """
        Returns the string representation of the message.
        :return: string representation of the message.
        :rtype: string.
        """
        return "\n".join(
            "{}: {}".format(k, v)
            for k, v in self.params.items()
        )

    def get_field_names(self):
        """
        Returns the params field names.
        :return: list of string.
        :rtype: list of string.
        """
        return self.params.keys()

    @classmethod
    def messages(cls):
        """
        Returns all the subclasses of Action Response.
        """
        return cls.__subclasses__()



class LoginMessage(ActionMessage):
    """
    docstring for LoginMessage.
    Login Action message.
    """
    def __init__(\
        self,\
        username,\
        secret,\
        actionId='1'\
    ):
        """
        Constructor.
        :param username: username of the asterisk user.
        :type username: string
        :param secret: password of the asterisk user.
        :type secret: string
        :param actionId: Id of the action message.
        :type actionId: string
        """
        super(LoginMessage, self).__init__(
            'Login',
            actionId
        )
        self.params['Username'] = username
        self.params['Secret'] = secret

class LogoffMessage(ActionMessage):
    """
    docstring for LogoffMessage.
    Logoff Action message.
    """
    def __init__(\
        self,\
        actionId='3'\
    ):
        """
        Constructor.

        :param actionId: Id of the action message.
        :type actionId: string

        """
        super(LogoffMessage, self).__init__(
            'Logoff',
            actionId
        )

class PingMessage(ActionMessage):
    """
    docstring for PingMessage.
    Ping Action message.
    """
    def __init__(\
        self,\
        actionId='3'\
    ):
        """
        Constructor.

        :param actionId: Id of the action message.
        :type actionId: string

        """
        super(PingMessage, self).__init__(
            'Ping',
            actionId
        )

class QueueStatusMessage(ActionMessage):
    """
    docstring for QueueStatusMessage.
    QueueStatus Action message.
    """
    def __init__(\
        self,\
        actionId='4'\
    ):
        """
        Constructor.

        :param actionId: Id of the action message.
        :type actionId: string

        """
        super(QueueStatusMessage, self).__init__(
            'QueueStatus',
            actionId
        )

def __constructor_arguments(class_name):
    initializer_info = inspect.getargspec(\
        class_name.__init__\
    )

    arguments = initializer_info[0]
    arguments.remove('self')
    return arguments



def __has_required_arguments(name, fields):
    class_name = globals()[name]
    for arg in __constructor_arguments(class_name):
        if not arg in fields:
            return False
    return True

def __is_an_action_message(name):
    names = map(
        lambda x: x.__name__,\
        ActionMessage.messages()
    )
    return name in names


MESSAGE_NAME_FORMAT = "{}Message"
ACTION_KEY = 'action'
def is_a_valid_message(fields):
    """
    Checks if the given data fields correspond
    to a valid action message.

    :param fields: fields of the message
    :type fields: dictionary

    :return: True if the message fields are valid.
    :rtype: bool.
    """
    if not ACTION_KEY in fields:
        return False
    name = MESSAGE_NAME_FORMAT.format(
        fields[ACTION_KEY]
    )
    if not __is_an_action_message(name):
        return False
    if not __has_required_arguments(name, fields):
        return False

    return True

def create_message(data):
    """
    Creates an action message.
    :param data: data to create the message with.
    :type data: dictionary

    :return: an action message.
    :rtype: ActionMessage.
    """
    name = MESSAGE_NAME_FORMAT.format(
        data[ACTION_KEY]
    )
    class_name = globals()[name]
    return class_name(
        **{
            k:data[k]
            for k in __constructor_arguments(
                class_name
            )
        }
    )
