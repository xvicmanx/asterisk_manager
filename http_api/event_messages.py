"""
Authors: Victor Trejo and Wally Trejo
Description: Event messages.
"""

class Event(object):
    """
    docstring for Event:
    Abstraction for Event messages.
    """
    def __init__(self, event_type):
        """
        Constructor.

        :param event_type: type of the event.
        :type event_type: string

        """
        super(Event, self).__init__()
        self.__event_type = event_type

    def __str__(self):
        """
        String representation of the event message.

        :return: string representation of the message.
        :rtype: string.

        """
        return "\n".join(\
            "{}: {}".format(k, v)
            for k, v in self.__dict__.items()
        )

    def get_event_type(self):
        """
        Gets the type of the event.
        :return: the type of the event.
        :rtype: string.
        """
        return self.__event_type


class GeneralEvent(Event):
    """
    docstring for GeneralEvent:
    General Type for the not matching events.
    """
    def __init__(self, eventType, fields):
        """
        Constructor.

        :param eventType: type of the event.
        :type eventType: string

        :param fields: event message fields.
        :type fields: dictionary
        """
        super(GeneralEvent, self).__init__(eventType)
        self.__fields = fields

    def get_fields(self):
        """
        Gets the fields of the event message.

        :return: the fields.
        :rtype: dictionary.

        """
        return self.__fields

    def __str__(self):
        """
        String representation of the event message.

        :return: string representation of the message.
        :rtype: string.

        """
        return "\n".join(\
            "{}: {}".format(k, v)
            for k, v in self.__fields.items()
        )



def event_for_type(event_type, fields):
    """
    Construct an Event object for the given type and
    fields data.

    :param eventType: type of event to create.
    :type eventType: string

    :param fields: fields data.
    :type fields: dictionary.
    :return: the constructed event.
    :rtype: Event.
    """
    return GeneralEvent(
        event_type,
        fields
    )
