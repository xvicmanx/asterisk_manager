#!/usr/bin/python3
"""
Authors: Victor Trejo and Wally Trejo
Description: Asterisk Telnet Manager.
"""
import threading
import telnetlib
from queue import Queue
import event_messages
import action_messages

# End of line separator.
EOL = '\r\n'

def fields_to_string(fields):
    """
    Converts a dictionary into string.
    :param fields: fields dictionary.
    :type fields: dictionary

    :return: the resulting string.
    :rtype: string.
    """
    return "\n".join(\
        "{}: {}".format(k, v)\
        for k, v in fields.items()\
    )

class ActionResponse(object):
    """
    docstring for ActionResponse:
    Represents a message response to an action.
    """
    RESPONSE_KEY = 'Response'
    ACTIONID_KEY = 'ActionID'

    def __init__(\
        self,\
        fields\
    ):
        super(ActionResponse, self).__init__()
        self.__fields = fields
        self.__action_id = fields.get(
            self.ACTIONID_KEY,
            ''
        )

    def get_fields(self):
        """
        Returns the fields of the message
        :return: a dictionary of the fields.
        :rtype: dictionary.
        """
        return self.__fields

    def get_action_id(self):
        """
        Returns action id of the message
        :return: the action id.
        :rtype: int.
        """
        return self.__action_id

    def __str__(self):
        """
        Returns the string representation of the response.
        :return: string representation of the response.
        :rtype: string.
        """
        return "\n".join(\
            "{}: {}".format(k, v)\
            for k, v in self.__fields.items()\
        )

class AsteriskTelnetClient(object):
    """
    docstring for AsteriskTelnetClient:
    Connects to the Asterisk Telnet
    Interface.
    """
    FIELDS_SEPARATOR = ":"
    ENCODING = "utf-8"

    def __init__(\
        self,\
        connection,\
        message_received_callback=lambda x: None,\
        separator=2 * EOL\
        ):
        """
        Constructor
        :param connection: telnet connection.
        :type connection: Telnet

        :param message_received_callback: callback for the received
                                        messages.
        :type message_received_callback: function.

        :param separator: messages separator.
        :type separator: string.
        """
        super(AsteriskTelnetClient, self).__init__()
        self.__received_messages_storage = Queue()
        self.__connection = connection
        self.__separator = separator
        self.__on_message_received_callback = message_received_callback
        self.__continue_looping = True
        self.__messages_reader = None
        self.__messages_dispatcher = None

    def set_message_received_callback(self, callback):
        """
        Sets the callback for the received messages.

        :param callback: callback for the received
                                        messages.
        :type callback: function.

        """
        self.__on_message_received_callback = callback


    def start(self):
        """
        Starts the client.
        Starts the messages reader and dispatcher
        threads.
        """
        self.__messages_reader = threading.Thread(\
            target=self.__read_messages\
        )
        self.__messages_dispatcher = threading.Thread(\
            target=self.__dispatch_received_messages\
        )

        self.__messages_dispatcher.start()
        self.__messages_reader.start()



    def stop_looping(self):
        """
        Stop the threads by cancelling
        the continuing looping condition.
        """
        self.__continue_looping = False


    def send_message(\
        self,\
        fields\
    ):
        """
        Send a message specified by the
        fields dictionary to the asterisk
        server.
        :param fields: fields dictionary.
        :type fields: dictionary
        """
        self.__connection.write(
            bytes("{}{}".format(
                fields_to_string(fields),
                self.__separator
            ), self.ENCODING)
        )


    def __read_messages(self):
        """
        Continually read messages from
        the asterisk server and stores
        them into the received messages store.
        """
        while self.__continue_looping:
            self.__received_messages_storage.put(
                self.__connection.read_until(
                    bytes(self.__separator, self.ENCODING)
                ).decode(self.ENCODING)
            )



    def __parse_fields(self, text):
        """
        Takes an asterisk message text and
        converts it into a dictionary.
        :param text: message's text.
        :type text: string

        :return: the parsed message.
        :rtype: dictionary.
        """

        fields = filter(\
            lambda x: len(x) == 2,\
            map(\
                lambda x: tuple(\
                    x.split(\
                        self.FIELDS_SEPARATOR\
                    )\
                ),\
                text.strip().split(EOL)\
            )\
        )
        result = ({
            key.strip():value.strip()
            for key, value in fields
        })

        return result



    def __dispatch_received_messages(self):
        """
        Reads the stored received messages,
        parses them and passes it to
        the onMessageReceivedCallback.
        """
        while self.__continue_looping:
            # Receiving any response or event.
            if not self.__received_messages_storage.empty():
                self.__on_message_received_callback(
                    self.__parse_fields(
                        self.__received_messages_storage.get()
                    )
                )


def print_result(result):
    """
    Prints a result out on the screen.
    param result: result to print.
    type result: object
    """
    print("\n### Message Received ###")
    print(result)



class AsteriskManager(object):
    """
    docstring for Manager
    Connects to an Asterisk server and
    sends and listens to messages and events.
    """
    EVENT_KEY = 'Event'
    def __init__(\
        self,\
        asterisk_client,\
        responses_default_handler=lambda x: None,\
        events_default_handler=lambda x: None\
    ):
        """
        Constructor.

        :param asterisk_client: Asterisk's telnet client.
        :type asterisk_client: AsteriskTelnetClient.

        :param responses_default_handler: default callback
                                        for all the non listened responses.
        :type responses_default_handler: function.

        :param events_default_handler: default callback
                                        for all the non listened events.
        :type events_default_handler: function.
        """
        super(AsteriskManager, self).__init__()
        self.__client = asterisk_client
        self.__responses_handlers = {}
        self.__events_handlers = {}
        self.__responses_default_handler = responses_default_handler
        self.__events_default_handler = events_default_handler


    def __parse_message(self, fields):
        """
        Parses a given message described by a dictionary of fields.

        :param fields: dictionary of fields.
        :type fields: dictionary.

        :return: an action message or event.
        :rtype: Message.
        """
        if ActionResponse.RESPONSE_KEY in fields.keys():
            return ActionResponse(fields)
        else:
            return event_messages.event_for_type(\
                fields[self.EVENT_KEY],\
                fields\
            )


    def __on_message_received(self, data):
        """
        Function called when a message is received.
        It parses the given message and pass it to
        the corresponding handler.

        :param data: dictionary of fields of the message.
        :type data: dictionary
        """
        message = self.__parse_message(data)
        if isinstance(message, ActionResponse):
            self.__responses_handlers.get(
                message.get_action_id(),
                self.__responses_default_handler
            )(message)
        elif isinstance(message, event_messages.Event):
            self.__events_handlers.get(
                message.get_event_type(),
                self.__events_default_handler
            )(message)


    def send_message(\
        self,\
        message,\
        response_callback=None\
    ):
        """
        Sends a message to the asterisk client.

        :param message: message to be sent.
        :type message: ActionMessage.

        :param response_callback: callback to receive the action response.
        :type response_callback: function.
        """
        if not response_callback is None:
            self.__register_action_response_callback(
                message.ActionID,
                response_callback
            )

        self.__client.send_message(
            message.params
        )


    def start(self):
        """
        Starts the Asterisk Manager.
        """
        self.__client.set_message_received_callback(
            self.__on_message_received
        )
        self.__client.start()


    def stop(self):
        """
        Stops the Asterisk Manager.
        """
        self.__client.stop_looping()
        self.__client.send_message(\
            action_messages.LogoffMessage().params\
        )

    def register_event_handler(self, event, callback):
        """
        Registers an event handler.

        :param event: event to handle.
        :type event: string.

        :param callback: handler function.
        :type callback: function.
        """
        self.__events_handlers[event] = callback

    def __register_action_response_callback(\
        self,\
        action,\
        callback\
    ):
        """
        Registers an action response handler.

        :param action: action response to handle.
        :type action: string.

        :param callback: handler function.
        :type callback: function.
        """
        self.__responses_handlers[action] = callback


def get_asterisk_manager(\
    host,\
    port,\
    responses_default_handler=lambda x: None,\
    events_default_handler=lambda x: None\
):
    """
    Connects to an Asterisk Manager at
    host and port.

    :param host: host of the asterisk manager telnet connection.
    :type host: string

    :param port: port of the asterisk manager telnet connection.
    :type port: int

    :param responses_default_handler: default callback
                                        for all the non listened responses.
    :type responses_default_handler: function.

    :param events_default_handler: default callback
                                    for all the non listened events.
    :type events_default_handler: function.

    :return: the manager.
    :rtype: AsteriskManager.
    """
    return AsteriskManager(
        AsteriskTelnetClient(
            telnetlib.Telnet(
                host,
                port
            )
        ),
        responses_default_handler,
        events_default_handler
    )


def start_and_login(\
    asterisk_manager,\
    username,\
    secret,\
    callback\
):
    """
    Starts and login to an Asterisk Manager a given asterisk manager.

    :param username: username of the asterisk user.
        :type username: string

    :param secret: password of the asterisk user.
    :type secret: string.

    :param callback: callback of the login action.
    :type callback: function
    """
    asterisk_manager.start()
    print("Sending Login Message...\n")
    asterisk_manager.send_message(
        action_messages.LoginMessage(
            username,
            secret
        ),
        callback
    )
