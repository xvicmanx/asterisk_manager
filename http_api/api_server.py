#!/usr/bin/python3
"""
Author: Victor Trejo
Description: This an API server to the asterisk telnet client.
"""
from functools import wraps
import json
import sys
import signal
import argparse
from threading import Thread
import requests
from flask import Flask, request
from flask_cors import CORS

import asterisk_manager as am
import action_messages
import event_messages

ACTION_REQUEST_KEY = 'action'
ACTION_ID_REQUEST_KEY = 'actionId'
EVENT_REQUEST_KEY = 'event'
EVENTS_REQUEST_KEY = 'events[]'
URL_REQUEST_KEY = 'url'
MESSAGE_RESPONSE_KEY = 'message'

manager = None
event_listeners = {}
response_listeners = {}

app = Flask(__name__)
CORS(app)

def __execute_in_thread(action):
    """
    Executes an function in a thread.
    :param action: function to be executed in thread.
    :type action: function
    """
    Thread(target=action).start()


def __notify(url, data):
    """
    Notifies webhook clients specified by an url
    with a request sending data to then.
    :param url: webhook url.
    :type url: string
    :param data: data to be sent.
    :type data: dictionary
    """
    try:
        requests.get(
            url,
            params=data
        )
    except Exception as exception:
        print(exception)


def __not_success(description):
    """
    Creates a not success JSON response object.
    :param description: description of the message.
    :type description: string
    """
    return json.dumps(
        {
            'success': False,
            'description': description
        }
    )

def __success(params):
    """
    Creates a not success JSON response object.
    :param description: description of the message.
    :type description: string
    """
    params["success"] = True
    return json.dumps(params)


def argument_included_in_request(key):
    """
    Function decorator: check that a sent request
    contains the necessary field.
    :param key: the key to be verified.
    :type key: string
    """
    def __decorator(function):
        @wraps(function)
        def __wrapper(*args, **kwargs):
            if not key in request.args:
                return __not_success(
                    "Missing key '{}'".format(key)
                )
            else: return function(*args, **kwargs)
        return __wrapper
    return __decorator


def __add_event_listener(event, listener):
    event_listeners[event] = event_listeners.get(
        event,
        []
    )

    if not listener in event_listeners[event]:
        event_listeners[event].append(listener)

    print("The Listener {} registered to {} event".format(\
            listener,\
            event\
        )\
    )


@argument_included_in_request(
    EVENTS_REQUEST_KEY
)
@argument_included_in_request(
    URL_REQUEST_KEY
)
@app.route(
    '/register-to-events',
    methods=['GET', 'POST']
)
def register_to_events():
    """
    API end point to register to events.
    It expects two arguments sent by the request:
        1. url, which is the webhook url.
        2. events, the events to listen to.
    """
    url = request.args.get(
        URL_REQUEST_KEY
    )
    events = request.args.getlist(
        EVENTS_REQUEST_KEY
    )
    for event in events:
        __add_event_listener(
            event,
            url
        )

    return __success({
        URL_REQUEST_KEY: url,
        EVENTS_REQUEST_KEY: events
    })



@argument_included_in_request(
    EVENT_REQUEST_KEY
)
@argument_included_in_request(
    URL_REQUEST_KEY
)
@app.route(\
    '/register-to-event',\
    methods=['GET', 'POST']\
)
def register_to_event():
    """
    API end point to register to event.
    It expects two arguments sent by the request:
        1. url, which is the webhook url.
        2. event, the event to listen to.
    """
    event = request.args.get(
        EVENT_REQUEST_KEY
    )
    url = request.args.get(
        URL_REQUEST_KEY
    )
    __add_event_listener(event, url)
    return __success({
        URL_REQUEST_KEY: url,
        EVENT_REQUEST_KEY: event
    })




@app.route(\
    '/send-action',\
    methods=['GET', 'POST']\
)
def send_action():
    """
    API end point to send an action to the asterisk manager.
    It expects three arguments sent by the request:
        1. url, which is the webhook url.
        2. action, action to send.
        3. actionId, action id of the action.
    """
    data = request.args
    valid = action_messages.is_a_valid_message(
        data
    )
    if not valid:
        return __not_success(
            'Invalid action custom params'
        )

    action_id = data[ACTION_ID_REQUEST_KEY]
    url = data[URL_REQUEST_KEY]
    response_listeners[action_id] = url
    message = action_messages.create_message(data)
    __execute_in_thread(
        lambda: manager.send_message(message)
    )

    return __success({
        URL_REQUEST_KEY: url,
        ACTION_REQUEST_KEY: data[ACTION_REQUEST_KEY],
        ACTION_ID_REQUEST_KEY: action_id
    })



def on_event(data):
    """
    Callback function called when an event is triggered.
    :param data: event data
    :type data: event_messages.Event
    """
    am.print_result(data)
    listeners = event_listeners.get(
        data.get_event_type(),
        []
    )
    fields = data.__dict__
    if isinstance(data, event_messages.GeneralEvent):
        fields = data.get_fields()

    def __notify_listeners():
        for listener in listeners:
            __notify(listener, fields)

    __execute_in_thread(
        __notify_listeners
    )


def on_response(data):
    """
    Callback function called when a response needs to
    be sent.
    :param data: response data
    :type data: asterisk_manager.ActionResponse
    """
    am.print_result(data)
    key = data.get_action_id()
    listener = response_listeners.get(key)

    if not listener is None:
        del response_listeners[key]
        __execute_in_thread(
            lambda: __notify(listener, data.get_fields())
        )


def exit_correctly(signum, frame):
    """
    Callback function called when the program is interrupt.
    :param signum: signal number.
    :type signum: int
    :param frame: frame.
    :type frame: object
    """
    manager.stop()
    print('Exiting correctly')
    sys.exit(0)



def main(\
    asterisk_host,\
    asterisk_port,\
    username,\
    user_password,\
    api_port):
    """
    Main function of the script.
    Connect to the asterisk server and
    initializes the API server.

    :param asterisk_host: host of the asterisk server.
    :type asterisk_host: string
    :param asterisk_port: port of the asterisk server.
    :type asterisk_port: int
    :param asterisk_port: port of the asterisk server.
    :type asterisk_port: int
    :param username: username of the user to connect to the server.
    :type username: string
    :param user_password: password of the user to connect to the server.
    :type user_password: string
    :param asterisk_port: port of the asterisk server.
    :type asterisk_port: int
    """
    global manager
    try:
        signal.signal(
            signal.SIGINT,
            exit_correctly
        )

        manager = am.get_asterisk_manager(
            asterisk_host,
            asterisk_port,
            on_response,
            on_event
        )

        print("Loging in to asterisk server")
        am.start_and_login(
            manager,
            username,
            user_password,
            am.print_result
        )

        app.run(port=api_port)

    except Exception as exception:
        print(
            "Exception: {}".format(exception)
        )
        manager.stop()



if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description='Initializes the api to the asterisk server.'
    )
    parser.add_argument(
        '--port',
        type=int,
        default=82,
        help='API serving port'
    )
    parser.add_argument(
        '--asterisk_host',
        type=str,
        help='The asterisk server host'
    )
    parser.add_argument(
        '--asterisk_port',
        type=int,
        default=25038,
        help='The asterisk server port'
    )
    parser.add_argument(
        '--username',
        type=str,
        help='The username'
    )
    parser.add_argument(
        '--secret',
        type=str,
        help="The user's password"
    )
    options = parser.parse_args()
    # print(options)
    main(
        options.asterisk_host,
        options.asterisk_port,
        options.username,
        options.secret,
        options.port
    )
